package es.ua.jtech.latas;

public class GameStatus {

	float score;
	int stage;
	float money;
	
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public int getStage() {
		return stage;
	}
	public void setStage(int stage) {
		this.stage = stage;
	}
	public void nextStage() {
		this.stage++;
	}
	public void addScore(float score) {
		this.score += score;
	}
	public String getScoreString() {
		return "" + ((int)score);
	}
	public float getMoney() {
		return money;
	}
	public void setMoney(float money) {
		this.money = money;
	}
	public void addMoney(float money) {
		this.money += money;
	}
	public String getMoneyString() {
		return String.format("%.2f", money);
	}
}
