package es.ua.jtech.latas.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import es.ua.jtech.latas.Game;
import es.ua.jtech.latas.RenderUtils;
import es.ua.jtech.latas.Game.SceneTransition;
import es.ua.jtech.latas.RenderUtils.TextHAlign;
import es.ua.jtech.latas.RenderUtils.TextVAlign;
import es.ua.jtech.latas.data.StringManager;

public class TitleScene implements Scene {

	SpriteBatch batch;
	
	int width;
	int height;
	
	@Override
	public void init() {
		width = Gdx.app.getGraphics().getWidth();
		height = Gdx.app.getGraphics().getHeight();		
		
		batch = new SpriteBatch();
	}
	
	@Override
	public void render() {
		String titleString = StringManager.getString(StringManager.ID_STRING_TITLE);

		RenderUtils.clearBackground(width, height);
		batch.begin();
		RenderUtils.renderText(batch, titleString, width/2, height/2, 
				TextHAlign.TEXT_H_ALIGN_CENTER, TextVAlign.TEXT_V_ALIGN_CENTER);
		batch.end();
	}

	@Override
	public SceneTransition update(float dt) {
		Input input = Gdx.app.getInput();
		
		if(input.justTouched()) {
			// Pone a 0 los marcadores y el nivel actual
			Game.status.setScore(0.0f);
			Game.status.setMoney(0.0f);
			Game.status.setStage(0);

			return SceneTransition.TRANSITION_GAME;
		} else {
			return SceneTransition.TRANSITION_NONE;			
		}
	}

	@Override
	public void resize(int w, int h) {
		this.width = w;
		this.height = h;
	}

	@Override
	public void dispose() {
		batch.dispose();
	}
}
