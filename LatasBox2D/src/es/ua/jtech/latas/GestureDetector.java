/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.ua.jtech.latas;

public class GestureDetector {

    // Tap timeout
    private final static int TAP_TIME = 250;
    
    // Distancia minima al cuadrado para considerar scroll
    private final static int TOUCH_SLOP = 10 * 10;
    
    // Velocidad minima para considerar evento fling
    private final static float MINIMUM_FLING_VELOCITY = 10.0f;

    // Listener de eventos de reconocimiento de gestos
    public interface OnGestureListener {
    	void onDown(int x, int y);
        void onSingleTap(int x, int y);
        void onScrollMove(int firstX, int firstY, int lastX, int lastY, float distanceX, float distanceY);
        void onScrollUp(int firstX, int firstY, int lastX, int lastY);
        void onFling(int firstX, int firstY, int lastX, int lastY, float velocityX, float velocityY);
    }

    private int firstX;
    private int firstY;
    private int lastX;
    private int lastY;
    private boolean isInTapRegion;
    private boolean isPressed;
    private long downTime;
    
    private final OnGestureListener listener;

    private VelocityTracker velocityTracker;

    public GestureDetector(OnGestureListener listener) {
        this.listener = listener;
        this.isPressed = false;
        this.velocityTracker = null;
    }
    
    // Se acaba de pulsar
    public void touchDown(int x, int y) {
    	if(!isPressed) {
        	downTime = System.currentTimeMillis();

        	if(velocityTracker==null) {
        		velocityTracker = VelocityTracker.obtain();
        	}
        	velocityTracker.addMovement(x,y, downTime);

        	listener.onDown(x, y);

        	firstX = lastX = x; 
        	firstY = lastY = y;
        	isInTapRegion = true;

        	isPressed = true;    		
    	}
    }
    
    // Se mantiene pulsado
    public void touchMove(int x, int y) {
    	if(isPressed) {
        	velocityTracker.addMovement(x, y, System.currentTimeMillis());
        	
        	final float scrollX = lastX - x;
            final float scrollY = lastY - y;
            if (isInTapRegion) {
                final int deltaX = (int) (x - firstX);
                final int deltaY = (int) (y - firstY);
                int distance = (deltaX * deltaX) + (deltaY * deltaY);
                if (distance > TOUCH_SLOP) {
                    listener.onScrollMove(firstX, firstY, lastX, lastY, scrollX, scrollY);
                    lastX = x;
                    lastY = y;
                    isInTapRegion = false;
                }
            } else if ((Math.abs(scrollX) >= 1) || (Math.abs(scrollY) >= 1)) {
                listener.onScrollMove(firstX, firstY, lastX, lastY, scrollX, scrollY);
                lastX = x;
                lastY = y;
            }    		
    	}
    }
    
    // No est� pulsado
    public void touchNone() {
    	if(isPressed) {
            if (isInTapRegion) {            	
            	long elapsedTime = System.currentTimeMillis() - downTime;
            	if(elapsedTime < TAP_TIME) {
                    listener.onSingleTap(lastX, lastY);        		
            	}
            } else {            	
                final VelocityTracker velocityTracker = this.velocityTracker;
                velocityTracker.computeCurrentVelocity(1000);
                final float velocityY = velocityTracker.getYVelocity();
                final float velocityX = velocityTracker.getXVelocity();

                if ((Math.abs(velocityY) > MINIMUM_FLING_VELOCITY)
                        || (Math.abs(velocityX) > MINIMUM_FLING_VELOCITY)){
                    listener.onFling(firstX, firstY, lastX, lastY, velocityX, velocityY);
                } else {
                	listener.onScrollUp(firstX, firstY, lastX, lastY);
                }
            }
            velocityTracker.recycle();
            velocityTracker = null;

            isPressed = false;
    	}
    }

}
