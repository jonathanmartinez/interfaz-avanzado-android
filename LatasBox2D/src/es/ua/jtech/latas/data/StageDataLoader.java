package es.ua.jtech.latas.data;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import es.ua.jtech.latas.utils.json.JSONArray;
import es.ua.jtech.latas.utils.json.JSONException;
import es.ua.jtech.latas.utils.json.JSONObject;

public class StageDataLoader {

	public static List<StageData> load(String file) {
		List<StageData> stages = new ArrayList<StageData>();
		String jsonText = Gdx.files.getFileHandle(file, FileType.Internal).readString();

		try {
			// Lee las fases
			JSONArray jsonArrayStages = new JSONArray(jsonText);
			for(int i=0;i<jsonArrayStages.length();i++) {
				StageData stage = new StageData();
				List<Vector2> cans = new ArrayList<Vector2>();
				List<Vector3> coins = new ArrayList<Vector3>();
				List<Brick> bricks = new ArrayList<Brick>();
				
				// Lee los datos de una fase
				JSONObject jsonObjectStage = (JSONObject)jsonArrayStages.get(i);
				String stageName = jsonObjectStage.getString("name");

				// Lee los datos de la bola
				JSONArray jsonArrayBall = (JSONArray)jsonObjectStage.get("ball");
				float x = (float)jsonArrayBall.getDouble(0);
				float y = (float)jsonArrayBall.getDouble(1);
				Vector2 ballPos = new Vector2(x, y);
				
				// Lee los datos de las latas
				JSONArray jsonArrayCans = (JSONArray)jsonObjectStage.get("cans");
				for(int j=0;j<jsonArrayCans.length();j++) {
					JSONObject jsonObjectCan = (JSONObject)jsonArrayCans.get(j);
					JSONArray jsonArrayPosition = (JSONArray)jsonObjectCan.get("position");
					x = (float)jsonArrayPosition.getDouble(0);
					y = (float)jsonArrayPosition.getDouble(1);
					cans.add(new Vector2(x, y));
				}

				// Lee los datos de las monedas
				JSONArray jsonArrayCoins = (JSONArray)jsonObjectStage.get("coins");
				for(int j=0;j<jsonArrayCoins.length();j++) {
					JSONObject jsonObjectCoin = (JSONObject)jsonArrayCoins.get(j);
					JSONArray jsonArrayPosition = (JSONArray)jsonObjectCoin.get("position");
					x = (float)jsonArrayPosition.getDouble(0);
					y = (float)jsonArrayPosition.getDouble(1);
					float value = (float)jsonObjectCoin.getDouble("value");
					coins.add(new Vector3(x, y, value));
				}
				
				// Lee los datos de los ladrillos
				if (jsonObjectStage.has("bricks")) {
					JSONArray jsonArrayBricks = (JSONArray)jsonObjectStage.get("bricks");
					for(int j=0;j<jsonArrayBricks.length();j++) {
						JSONObject jsonObjectBrick = (JSONObject)jsonArrayBricks.get(j);
						Brick b = new Brick();
						JSONArray jsonArrayPosition = (JSONArray)jsonObjectBrick.get("position");
						b.position.x = (float)jsonArrayPosition.getDouble(0);
						b.position.y = (float)jsonArrayPosition.getDouble(1);
						JSONArray jsonArraySize = (JSONArray)jsonObjectBrick.get("size");
						b.size.x = (float)jsonArraySize.getDouble(0);
						b.size.y = (float)jsonArraySize.getDouble(1);
						JSONArray jsonArrayColor = (JSONArray)jsonObjectBrick.get("color");
						b.color.x = (float)jsonArrayColor.getDouble(0);
						b.color.y = (float)jsonArrayColor.getDouble(1);
						b.color.z = (float)jsonArrayColor.getDouble(2);
						bricks.add(b);
					}
				}
				
				// Lee los datos de la imagen de fondo si tiene el atributo "bg"
				if (jsonObjectStage.has("bg"))
					stage.setBGName("datos/" + jsonObjectStage.getString("bg"));
				else
					stage.setBGName(null);

				stage.setStageName(stageName);
				stage.setBallPosition(ballPos);
				stage.setCanPositions(cans);
				stage.setCoinPositions(coins);
				stage.setBrickList(bricks);
				stages.add(stage);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return stages;
	}
	
}
