package es.ua.jtech.latas.data;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class StringManager {
	public final static String ID_STRING_TITLE = "titulo";
	public final static String ID_STRING_START = "inicio";
	public final static String ID_STRING_END = "fin";
	
	private static final String BUNDLE_NAME = "strings"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
