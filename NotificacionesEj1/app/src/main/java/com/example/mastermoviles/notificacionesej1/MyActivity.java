package com.example.mastermoviles.notificacionesej1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        Button b = (Button)(findViewById(R.id.boton));
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText e = (EditText)(findViewById(R.id.texto));
                if(e.getText().toString().equals("")){
                    Toast toast3 = new Toast(getApplicationContext());
                    toast3.setGravity(Gravity.CENTER| Gravity.CENTER,0,0);
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast_layout,
                            (ViewGroup) findViewById(R.id.lytLayout));
                    TextView txtMsg = (TextView)layout.findViewById(R.id.txtMensaje);
                    txtMsg.setText("Escribe un texto");
                    toast3.setDuration(Toast.LENGTH_SHORT);
                    toast3.setView(layout);
                    toast3.show();
                }
                else{

                    Toast toast3 = new Toast(getApplicationContext());
                    toast3.setGravity(Gravity.CENTER| Gravity.CENTER,0,0);
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast_layout,
                            (ViewGroup) findViewById(R.id.lytLayout));
                    TextView txtMsg = (TextView)layout.findViewById(R.id.txtMensaje);
                    txtMsg.setText(e.getText().toString());
                    toast3.setDuration(Toast.LENGTH_SHORT);
                    toast3.setView(layout);
                    toast3.show();

                }
                e.setText("");
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
