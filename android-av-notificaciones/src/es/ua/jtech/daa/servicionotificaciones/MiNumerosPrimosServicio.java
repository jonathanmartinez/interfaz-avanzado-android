package es.ua.jtech.daa.servicionotificaciones;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MiNumerosPrimosServicio extends Service {
	MiTarea miTarea;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Toast.makeText(this,"Servicio de primos creado ...", Toast.LENGTH_LONG).show();
		Log.i("SRV","onCreate");
		miTarea = new MiTarea();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("SRV", "onStartCommand");
		miTarea.execute();
		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this,"Servicio de primos destruido ...", Toast.LENGTH_LONG).show();
		Log.i("SRV","Servicio destruido");
		miTarea.cancel(true);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public static void cerrarMiNotificacion(NotificationManager nm){
		nm.cancel(MiTarea.NOTIF_ID);
	}
	
	private class MiTarea extends AsyncTask<String, String, String>{
		private long i;
		private static final long MAXCOUNT = 100000;
		public static final int NOTIF_ID = 1;
		Notification notification;
		NotificationManager notificationManager;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			i = 1;
			//TODO obtener el notificationManager a partri del servicio del sistema Context.NOTIFICATION_SERVICE
            String ns = Context.NOTIFICATION_SERVICE;
            notificationManager = (NotificationManager) getSystemService(ns);
			//TODO crear una Notification con los parámetros R.drawable.icon, "Primo descubierto", System.currentTimeMillis()
            long hora = System.currentTimeMillis();
            notification = new Notification(R.drawable.icon, "Primo descubierto", hora);

		}

		@Override
		protected String doInBackground(String... params) {
			for(i=2; i<MAXCOUNT; i++){ 
				if(esPrimo(i)){
					publishProgress(""+i);
					Log.i("SRV", "Primo descubierto: "+i);
				}
			}
			return null;
		}

		private boolean esPrimo(long n) {
			boolean primo = true;
			for(long j = 2; j*j <= n; j++){
				if( i>= MAXCOUNT){
					primo = false; //abortar si cancelado.
					break;
				}
				try { //Pausar un poco
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	            if (n % j == 0) {
	                primo = false;
	                break;
		        }
			}
			return primo;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			//TODO crear un nuevo PendingIntent con el método estático PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
			//donde el notification intent es un intent para abrir la actividad principal.
            Context contexto = getApplicationContext();
            CharSequence titulo = "Primo descubierto";
            CharSequence descripcion = values[0] + "es el ultimo primo descubierto.";

            Intent notifIntent = new Intent(contexto,
                    AndroidNotificacionesActivity.class);

            PendingIntent contIntent = PendingIntent.getActivity(
                    contexto, 0, notifIntent, 0);


			//TODO poner en notification.setLatestEventInfo la cadena "Primo descubierto", values[0]+" es el último primo descubierto.",
			//pasándole el pendingIntent creado.

            notification.setLatestEventInfo(
                    contexto, titulo, descripcion, contIntent);
			
			//TODO hacer el notificationManager.notify de la notificación, con el NOTIF_ID
            //Enviar notificación
            notManager.notify(NOTIF_ID, notification);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			i = MAXCOUNT;
		}
		
	}
}
