package es.ua.jtech.latas.simulation;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class CanSprite extends Sprite {

	public final static int CAN_WIDTH = 32;
	public final static int CAN_HEIGHT = 60;
	
	public CanSprite(TextureRegion region) {
		super(region);
		this.setOrigin(CAN_WIDTH / 2, CAN_HEIGHT / 2);
	}
	
	
}
