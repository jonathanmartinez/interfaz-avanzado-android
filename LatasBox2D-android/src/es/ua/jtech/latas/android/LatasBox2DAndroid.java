package es.ua.jtech.latas.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;

import es.ua.jtech.latas.Game;

public class LatasBox2DAndroid extends AndroidApplication {
    /** Se invoca cuando se carga la aplicación por primera vez. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(new Game(), false);   
    }
}