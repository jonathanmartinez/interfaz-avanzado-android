package es.ua.jtech.latas;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;

import es.ua.jtech.latas.data.Assets;
import es.ua.jtech.latas.scenes.GameScene;
import es.ua.jtech.latas.scenes.Scene;
import es.ua.jtech.latas.scenes.TitleScene;

public class Game implements ApplicationListener {

	public static enum SceneTransition {
		TRANSITION_NONE,
		TRANSITION_TITLE,
		TRANSITION_GAME
	};
	
	public static Assets assets;
	public static GameStatus status;
	
	Scene currentState;	
	boolean isPaused;

	@Override
	public void create() {
		assets = new Assets();
		assets.load();
		
		status = new GameStatus();
		
		this.handleStateChange(SceneTransition.TRANSITION_TITLE);
	}

	@Override
	public void pause() {
		isPaused = true;
	}

	@Override
	public void resume() {
		isPaused = false;
	}
	
	@Override
	public void dispose() {
		currentState.dispose();
		assets.dispose();
	}

	@Override
	public void resize(int width, int height) {
		currentState.resize(width, height);
	}

	@Override
	public void render() {
		if(!isPaused && currentState!=null) {
			
			SceneTransition change = currentState.update(Gdx.app.getGraphics().getDeltaTime()); 
			this.handleStateChange(change);

			currentState.render();
		}

	}
	
	private void handleStateChange(SceneTransition change) {
		if(change!=SceneTransition.TRANSITION_NONE) {
			// Liberamos recursos de la escena actual
			if(currentState!=null) {
				currentState.dispose();
			}
			
			// Instanciamos nueva escena
			switch(change) {
			case TRANSITION_TITLE:
				currentState = new TitleScene();
				break;
			case TRANSITION_GAME:
				currentState = new GameScene();
				break;
			}
			
			// Inicializamos la nueva escena
			currentState.init();
		}
	}
	
	
}
