package es.ua.jtech.av.onboot;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.widget.Toast;

import java.lang.Override;

public class MiServicio extends Service implements BroadcastReceiver{
	Context context;

    @Override
    public void onReceive(){
        if( "android.intent.action.BOOT_COMPLETED".equals(intent.getAction()))
        {
            ComponentName comp = new ComponentName(context.getPackageName(),
                    MiServicio.class.getName());
            ComponentName service = context.startService(
                    new Intent().setComponent(comp));
            if (null == service){
                Log.e(MiBroadcastReceiver.class.toString(),
                        "Servicio no iniciado: " + comp.toString());
            }
        } else {
            Log.e(MiBroadcastReceiver.class.toString(),
                    "Intent no esperado " + intent.toString());
        }
    }


	@Override
	public void onCreate() {
		super.onCreate();
		Toast.makeText(this, "Servicio creado...", Toast.LENGTH_LONG).show();
		this.context = getApplicationContext();
		MiTarea t = new MiTarea();
		t.execute(5);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this, "Servicio destruido...", Toast.LENGTH_LONG).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	class MiTarea extends AsyncTask<Integer, Integer, Integer>{

		@Override
		protected Integer doInBackground(Integer... params) {
			int i=0;
			try {
				for(i=1; i<= params[0]; i++ ){
					Thread.sleep(2000);
					publishProgress(i);
				}
			} catch (InterruptedException e) {
			}
			return new Integer(i);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			Toast.makeText(context, ""+values[0], Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			stopSelf(); // El servicio
		}

		
	}
	
}
