package es.ua.jtech.latas.simulation;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Box2DFactory {

	/**
	 * Numero de pixels que corresponden a un metro en Box2D
	 */
	public final static float PTM_RATIO = 32;
	
	/**
	 * Crea un mundo con gravedad -10 en la coordenada y.
	 * @return Nuevo mundo Box2D
	 */
	public static World createWorld() {	
		World world = new World(new Vector2(0, -10), true);

		return world;
	}
	
	/**
	 * Crea los l�mites del mundo como bloques est�ticos en forma de rect�ngulo.
	 * @param world Mundo Box2D
	 * @param x Coordenada x inicial de los l�mites en pixels
	 * @param y Coordenada y inicial de los l�mites en pixels
	 * @param width Anchura de los l�mites en pixels
	 * @param height Altura de los l�mites en pixels
	 * @return Cuerpo Box2D que representa los l�mites del escenario
	 */
	public static Body createBounds(World world, float x, float y, float width, float height) {
		BodyDef limitesBodyDef = new BodyDef();
		limitesBodyDef.position.x = x;
		limitesBodyDef.position.y = y;
				
	    Body limitesBody = world.createBody(limitesBodyDef);
		PolygonShape limitesShape = new PolygonShape();
		limitesShape.setAsEdge(new Vector2(0.0f / PTM_RATIO, 0.0f / PTM_RATIO), 
				new Vector2(width / PTM_RATIO, 0.0f / PTM_RATIO));
		limitesBody.createFixture(limitesShape,0).setFriction(2.0f);

		limitesShape.setAsEdge(new Vector2(width / PTM_RATIO, 0.0f / PTM_RATIO), 
				new Vector2(width / PTM_RATIO, height / PTM_RATIO));
		limitesBody.createFixture(limitesShape,0);

		limitesShape.setAsEdge(new Vector2(width / PTM_RATIO, height / PTM_RATIO), 
				new Vector2(0.0f / PTM_RATIO, height / PTM_RATIO));
		limitesBody.createFixture(limitesShape,0);

		limitesShape.setAsEdge(new Vector2(0.0f / PTM_RATIO, height / PTM_RATIO), 
				new Vector2(0.0f / PTM_RATIO, 0.0f / PTM_RATIO));
		limitesBody.createFixture(limitesShape,0);
		
		return limitesBody;
	
	}
	
	/**
	 * Crea una lata con Box2D, como un rect�ngulo.
	 * @param world Mundo Box2D
	 * @param x Coordenada x inicial de la lata en pixels
	 * @param y Coordenada y inicial de la lata en pixels
	 * @param width Anchura de la lata en pixels
	 * @param height Altura de la lata en pixels
	 * @return Cuerpo Box2D de la lata
	 */
	public static Body createCan(World world, float x, float y, float width, float height) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;	
		bodyDef.position.x = x / PTM_RATIO;
		bodyDef.position.y = y / PTM_RATIO;

		Body body = world.createBody(bodyDef);
		
		PolygonShape bodyShape = new PolygonShape();
		bodyShape.setAsBox((width/2) / PTM_RATIO, (height/2) / PTM_RATIO);
		body.createFixture(bodyShape, 1.0f);
		bodyShape.dispose();		
		
		return body;
	}
	
	/**
	 * Crea una bola con Box2D, como un c�rculo
	 * @param world Mundo Box2D
	 * @param x Coordenada x inicial de la bola en pixels
	 * @param y Coordenada y inicial de la bola en pixels
	 * @param radius Radio de la bola en pixels
	 * @return Cuerpo Box2D de la bola
	 */
	public static Body createBall(World world, float x, float y, float radius) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.x = x / PTM_RATIO;
		bodyDef.position.y = y / PTM_RATIO;
		bodyDef.linearDamping = 1.0f; // 0.5f
		
		Body body = world.createBody(bodyDef);
		
		Shape bodyShape = new CircleShape();
		bodyShape.setRadius(radius / PTM_RATIO);		
		Fixture bodyFixture = body.createFixture(bodyShape, 3.0f);
		bodyFixture.setRestitution(0.5f);
		bodyShape.dispose();
		
		return body;
	}
	
	/**
	 * Crea una moneda con Box2D, como un círculo
	 * @param world Mundo Box2D
	 * @param x Coordenada x inicial de la moneda en pixels
	 * @param y Coordenada y inicial de la moneda en pixels
	 * @param radius Radio de la moneda en pixels
	 * @return Cuerpo Box2D de la moneda
	 */
	public static Body createCoin(World world, float x, float y, float radius) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.x = x / PTM_RATIO;
		bodyDef.position.y = y / PTM_RATIO;
		
		Body body = world.createBody(bodyDef);
		
		Shape bodyShape = new CircleShape();
		bodyShape.setRadius(radius / PTM_RATIO);		
		body.createFixture(bodyShape, 1.0f);
		bodyShape.dispose();
		
		return body;
	}
	
	public static Body createBrick(World world, float x, float y, float width, float height) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;	
		bodyDef.position.x = x / PTM_RATIO;
		bodyDef.position.y = y / PTM_RATIO;

		Body body = world.createBody(bodyDef);
		
		PolygonShape bodyShape = new PolygonShape();
		bodyShape.setAsBox((width/2) / PTM_RATIO, (height/2) / PTM_RATIO);
		body.createFixture(bodyShape, 1.0f);
		bodyShape.dispose();		
		
		return body;
	}
}
