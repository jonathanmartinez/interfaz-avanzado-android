package es.ua.jtech.latas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;


public class RenderUtils {
				
	public static enum TextHAlign {
		TEXT_H_ALIGN_LEFT,
		TEXT_H_ALIGN_CENTER,
		TEXT_H_ALIGN_RIGHT
	}

	public static enum TextVAlign {
		TEXT_V_ALIGN_TOP,
		TEXT_V_ALIGN_CENTER,
		TEXT_V_ALIGN_BOTTOM
	}
	
	public static void clearBackground(int width, int height) {
		GL10 gl = Gdx.app.getGraphics().getGL10();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glViewport(0, 0, width, height);
	}
	
	public static void renderText(SpriteBatch batch, String str, int x, int y) {
		Game.assets.font.draw(batch, str, x, y);		
	}

	public static void renderText(SpriteBatch batch, String str, int x, int y, TextHAlign hAlign, TextVAlign vAlign) {
		TextBounds bounds = getTextBounds(str);
		
		switch(hAlign) {
		case TEXT_H_ALIGN_CENTER:
			x -= bounds.width / 2;
			break;
		case TEXT_H_ALIGN_LEFT:
			break;
		case TEXT_H_ALIGN_RIGHT:
			x -= bounds.width;
			break;
		}
		
		switch(vAlign) {
		case TEXT_V_ALIGN_BOTTOM:
			y += bounds.height;
			break;
		case TEXT_V_ALIGN_CENTER:
			y += bounds.height / 2;
			break;
		case TEXT_V_ALIGN_TOP:
			break;
		}
		
		renderText(batch, str, x, y);
	}

	public static TextBounds getTextBounds(String str) {
		return Game.assets.font.getBounds(str);
	}
}
