package es.ua.jtech.av.serviciocontador;

import es.ua.jtech.daa.serviciocontador.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Main extends Activity {
	Main main;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        main = this;
        
        ((Button)findViewById(R.id.Button01)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startService(new Intent(main, MiCuentaServicio.class));
			}
		});
        ((Button)findViewById(R.id.Button02)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopService(new Intent(main,MiCuentaServicio.class));
			}
		});
    }

}