package com.example.mastermoviles.notificacionesej3;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ZoomControls;


public class MyActivity extends ActionBarActivity {
    int flagColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        flagColor = 0;

        ZoomControls zm = (ZoomControls)(findViewById(R.id.boton_zoom));
        zm.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView t = (TextView)(findViewById(R.id.texto1));
                float tam = t.getTextSize() - 0.5f;
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, tam);
            }
        });

        zm.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView t = (TextView)(findViewById(R.id.texto1));
                float tam = t.getTextSize() + 0.5f;
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, tam);
            }
        });


        Button b = (Button)(findViewById(R.id.boton_color));
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView t = (TextView)(findViewById(R.id.texto1));
                if(flagColor == 0){
                    t.setBackgroundColor(Color.WHITE);
                    t.setTextColor(Color.BLACK);
                    flagColor = 1;
                }
                else if(flagColor == 1){
                    t.setBackgroundColor(Color.BLACK);
                    t.setTextColor(Color.WHITE);
                    flagColor = 2;
                }
                else {
                    t.setBackgroundColor(Color.BLACK);
                    t.setTextColor(Color.GREEN);
                    flagColor = 0;
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
