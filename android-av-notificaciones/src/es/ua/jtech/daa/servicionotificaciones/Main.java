package es.ua.jtech.daa.servicionotificaciones;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Main extends Activity {
	Main main;
	NotificationManager notificationManager;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        main = this;
		notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        
        ((Button)findViewById(R.id.Button01)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startService(new Intent(main, MiNumerosPrimosServicio.class));
			}
		});
        ((Button)findViewById(R.id.Button02)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		    	//stopService(new Intent(main,miServicio.getClass()));
				stopService(new Intent(main,MiNumerosPrimosServicio.class));
			}
		});
        
        
    }

	@Override
	protected void onResume() {
		super.onResume();
		//TODO llamar al método estático de MiNumerosPrimosServicio que cierra la notificación
        MiNumerosPrimosServicio.cerrarMiNotificacion(notificationManager);

	}
    
   
}