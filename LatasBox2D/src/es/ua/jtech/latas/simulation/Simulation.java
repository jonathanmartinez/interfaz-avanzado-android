package es.ua.jtech.latas.simulation;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import es.ua.jtech.latas.Game;
import es.ua.jtech.latas.data.Brick;
import es.ua.jtech.latas.data.StageData;

public class Simulation implements ContactListener {

	private final static float IMPULSE_MUL = 100;
	
	World world;
	Body ballBody;
	List<Body> canBodies;
	List<Body> coinBodies;
	ArrayList<Body> brickBodies;
		
	Vector2 hitPoint;
	Vector2 impulse;

	List<Body> removeCoins;
	
	float damage;
	
	/**
	 * Crea la simulaci�n proporcionando los datos del escenario.
	 * @param stage
	 */
	public Simulation(StageData stage) {
		this.initWorld(stage);
	}
 	
	/**
	 * Inicializa el mundo a partir de los datos del escenario.
	 * @param stage
	 */
	private void initWorld(StageData stage) {
		// Crea el mundo para simulaci�n f�sica 2D
		world = Box2DFactory.createWorld();
		world.setContactListener(this);
		
		// Crea l�mites del escenario
		Box2DFactory.createBounds(world, 0.0f, 0.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		// Inicializa bola
		ballBody = Box2DFactory.createBall(world, stage.getBallPosition().x, 
				stage.getBallPosition().y, BallSprite.BALL_RADIUS);
		ballBody.setUserData(new BallSprite(Game.assets.ballRegion));
		ballBody.setType(BodyType.StaticBody);
		
		// Inicializa latas
		canBodies = new ArrayList<Body>();
		for(Vector2 v: stage.getCanPositions()) {
			Body canBody = Box2DFactory.createCan(world, v.x, v.y, CanSprite.CAN_WIDTH, CanSprite.CAN_HEIGHT);
			CanSprite can = new CanSprite(Game.assets.canRegion);
			canBody.setUserData(can);
			canBodies.add(canBody);
		}

		// Inicializa monedas
		coinBodies = new ArrayList<Body>();
		for(Vector3 v: stage.getCoinPositions()) {
			Body coinBody = Box2DFactory.createCoin(world, v.x, v.y, CoinSprite.COIN_RADIUS);
			CoinSprite coin = new CoinSprite(Game.assets.coinRegion, v.z); // En z habiamos almacenados el valor de la moneda
			coinBody.setUserData(coin);
			coinBodies.add(coinBody);
		}
		
		brickBodies = new ArrayList<Body>();
		for(Brick b: stage.getBrickList()) {
			Body brickBody = Box2DFactory.createBrick(world, b.position.x, b.position.y, b.size.x, b.size.y);
			BrickSprite brick = new BrickSprite((int)b.size.x, (int)b.size.y, b.color.x, b.color.y, b.color.z);
			brickBody.setUserData(brick);
			brickBodies.add(brickBody);
		}
		
		Vector2 pos;
		float rot;
		// Las monedas son objetos estáticos así que se posicionan aquí una vez
		for(Body coinBody: coinBodies) {
			CoinSprite coin = (CoinSprite)coinBody.getUserData();
			pos = coinBody.getPosition();
			rot = (float)Math.toDegrees(coinBody.getAngle());
			coin.setPosition(pos.x * Box2DFactory.PTM_RATIO - CoinSprite.COIN_RADIUS / 2,
					         pos.y * Box2DFactory.PTM_RATIO - CoinSprite.COIN_RADIUS / 2);
			coin.setRotation(rot);
		}

		// Los ladrillos son objetos estáticos así que se posicionan aquí una vez
		for(Body brickBody: brickBodies) {
			BrickSprite brick = (BrickSprite)brickBody.getUserData();
			pos = brickBody.getPosition();
			rot = (float)Math.toDegrees(brickBody.getAngle());
			brick.setPosition(pos.x * Box2DFactory.PTM_RATIO - brick.getWidth() / 2,
					          pos.y * Box2DFactory.PTM_RATIO - brick.getHeight() / 2);
			brick.setRotation(rot);
		}

		// Inicializa vectores auxiliares
		hitPoint = new Vector2();
		impulse = new Vector2();
		
		// Monedas a eliminar (se han detectado durante la colisión)
		removeCoins = new ArrayList<Body>();
		
		// Inicializa a 0 el da�o recibido por las latas
		damage = 0.0f;
	}
	
	public void setGravity(float x, float y) {
		Vector2 g = this.world.getGravity();
		g.set(x, y);
		this.world.setGravity(g);
	}
	
	/**
	 * Devuelve el sprite de la bola.
	 * @return Sprite de la bola
	 */
	public BallSprite getBall() {
		return (BallSprite)ballBody.getUserData();
	}
	
	/**
	 * Devuelve la lista de sprites de latas que hay en el escenario.
	 * @return Lista de todos los sprites de latas
	 */
	public List<CanSprite> getCans() {
		List<CanSprite> cans = new ArrayList<CanSprite>();
		for(Body canBody: canBodies) {
			cans.add((CanSprite)canBody.getUserData());
		}
		return cans;
	}
	
	/**
	 * Devuelve la lista de sprites de monedas que hay en el escenario.
	 * @return Lista de todos los sprites de monedas
	 */
	public List<CoinSprite> getCoins() {
		List<CoinSprite> coins = new ArrayList<CoinSprite>();
		for(Body coinBody: coinBodies) {
			coins.add((CoinSprite)coinBody.getUserData());
		}
		return coins;
	}
	
	/**
	 * Devuelve la lista de sprites de ladrillos que hay en el escenario.
	 * @return Lista de todos los sprites de ladrillos
	 */
	public List<BrickSprite> getBricks() {
		List<BrickSprite> bricks = new ArrayList<BrickSprite>();
		for(Body brickBody: brickBodies) {
			bricks.add((BrickSprite)brickBody.getUserData());
		}
		return bricks;
	}
	
	/**
	 * Golpea la bola en el punto indicado para lanzarla. Las coordenadas indicadas son
	 * coordenadas globales del mundo. La bola se convierte en objeto din�mico.
	 * @param x Coordenada x del mundo que recibe el impacto
	 * @param y Coordenada y del mundo que recibe el impacto
	 * @return <code>true</code> si las coordenadas especificadas est�n dentro de la bola,
	 * y <code>false</code> en caso contrario
	 */
	public boolean hit(int x, int y) {
		hitPoint.set((float)x / Box2DFactory.PTM_RATIO, (float)y / Box2DFactory.PTM_RATIO);
		Fixture ballFixture = ballBody.getFixtureList().get(0);
		
		if(ballFixture.testPoint(hitPoint)) {
			Vector2 ballCenter = ballBody.getWorldCenter();

			impulse.set(ballCenter);
			impulse.sub(hitPoint);
			impulse.mul(IMPULSE_MUL);
			
			ballBody.setType(BodyType.DynamicBody);
			ballBody.applyLinearImpulse(impulse, ballCenter);
			
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Lanza la bola con la velocidad indicada, convierti�ndola en objeto din�mico.
	 * @param vx Velocidad en x
	 * @param vy Velocidad en y
	 */
	public void launch(float vx, float vy) {
		impulse.set(vx / Box2DFactory.PTM_RATIO, vy / Box2DFactory.PTM_RATIO);
		Vector2 ballCenter = ballBody.getWorldCenter();
		ballBody.setType(BodyType.DynamicBody);
		ballBody.applyLinearImpulse(impulse, ballCenter);
	}
	
	/**
	 * Posiciona la bola en las coordenadas del mundo indicadas.
	 * @param x Coordenada x del mundo
	 * @param y Coordenada y del mundo
	 */
	public void setBallPosition(float x, float y) {
		hitPoint.set(x / Box2DFactory.PTM_RATIO, y / Box2DFactory.PTM_RATIO);
		ballBody.setTransform(hitPoint, 0);
	}
	
	/**
	 * Comprueba si las coordenadas del mundo indicadas corresponden a la posici�n
	 * donde est� la bola
	 * @param x Coordenada x del mundo
	 * @param y Coordenada y del mundo
	 * @return <code>true</code> si la bola se encuentra en esa posici�n, y <code>false</code>
	 * en caso contrario
	 */
	public boolean testBallPosition(float x, float y) {
		hitPoint.set((float)x / Box2DFactory.PTM_RATIO, (float)y / Box2DFactory.PTM_RATIO);
		Fixture ballFixture = ballBody.getFixtureList().get(0);
		return ballFixture.testPoint(hitPoint);
	}
	
	/**
	 * Actualiza las físicas del mundo con el intervalo de tiempo proporcionado.
	 * @param delta Intervalo de tiempo transcurrido
	 * @return <code>true</code> si ha cambiado algo del mundo (han desaparecido monedas), y <code>false</code> en caso contrario
	 */
	public boolean updatePhysics(float delta) {
		world.step(delta, 6, 2);
		world.clearForces();
		
		Vector2 pos;
		float rot;
		
		// Actualiza datos bola
		BallSprite ball = (BallSprite)ballBody.getUserData();
		pos = ballBody.getPosition();
		rot = (float)Math.toDegrees(ballBody.getAngle());
		ball.setPosition(pos.x * Box2DFactory.PTM_RATIO - BallSprite.BALL_RADIUS, 
				         pos.y * Box2DFactory.PTM_RATIO - BallSprite.BALL_RADIUS);
		ball.setRotation(rot);
		
		// Actualiza datos latas
		for(Body canBody: canBodies) {
			CanSprite can = (CanSprite)canBody.getUserData();
			pos = canBody.getPosition();
			rot = (float)Math.toDegrees(canBody.getAngle());
			can.setPosition(pos.x * Box2DFactory.PTM_RATIO - CanSprite.CAN_WIDTH / 2,
					        pos.y * Box2DFactory.PTM_RATIO - CanSprite.CAN_HEIGHT / 2);
			can.setRotation(rot);
		}
		
		// Eliminación de las monedas de la lista de candidatas a eliminar
		for(Body removeCoin: removeCoins) {
			coinBodies.remove(removeCoin);
			world.destroyBody(removeCoin);
		}
		boolean returned = removeCoins.size() > 0;
		removeCoins.clear();
		return returned;
	}
	
	/**
	 * Comprueba si la bola ha terminado de moverse.
	 * @return <code>true</code> si la bola ha quedado dormida, <code>false</code> en 
	 * caso contrario
	 */
	public boolean hasFinished() {
		return !ballBody.isAwake();
	}
	
	/**
	 * Devuelve la cantidad de da�o total infligida a las latas
	 * @return Cantidad de da�o que han recibido las latas
	 */
	public float getCurrentDamage() {
		return damage;
	}

	/**
	 * Pone a 0 la cantidad de da�o total acumulada
	 */
	public void resetCurrentDamage() {
		damage = 0.0f;
	}

	/**
	 * Libera la memoria ocupada por la simulaci�n del mundo
	 */
	public void dispose() {
		world.dispose();
	}

	/**
	 * Comprueba las colisiones con las latas y actualiza la cantidad de da�o en funci�n
	 * de la velocidad a la que colisionan.
	 */
	@Override
	public void beginContact(Contact c) {
		Body bodyA = c.getFixtureA().getBody();
		Body bodyB = c.getFixtureB().getBody();
		
		// Obtiene el punto de contacto
		Vector2 point = c.GetWorldManifold().getPoints()[0];
		
		// Calcula la velocidad a la que se produce el impacto
		Vector2 velA = bodyA.getLinearVelocityFromWorldPoint(point);
		Vector2 velB = bodyB.getLinearVelocityFromWorldPoint(point);
		
		float vel = c.GetWorldManifold().getNormal().dot(velA.sub(velB));
		
		// Si alguno de los cuerpos implicados en el contacto es una lata, incrementa el da�o
		if(bodyA.getUserData() !=null && bodyA.getUserData() instanceof CanSprite) {
			damage += vel;
		}
		if(bodyB.getUserData() !=null && bodyB.getUserData() instanceof CanSprite) {
			damage += vel;
		}
		
		// Si hay un contacto entre dos cuerpos 
		if (bodyA.getUserData() != null && bodyB.getUserData() != null) {
			// Y uno es una bola y otro es una moneda
			if (bodyA.getUserData() instanceof BallSprite && bodyB.getUserData() instanceof CoinSprite) {
				CoinSprite coin = (CoinSprite)bodyB.getUserData();
				Game.status.addMoney(coin.value);
				removeCoins.add(bodyB);
			}
			// O al revés
			if (bodyA.getUserData() instanceof CoinSprite && bodyB.getUserData() instanceof BallSprite) {
				CoinSprite coin = (CoinSprite)bodyA.getUserData();
				Game.status.addMoney(coin.value);
				removeCoins.add(bodyA);
			}
		}
	}

	/**
	 * El contacto entre dos cuerpos ha finalizado.
	 */
	@Override
	public void endContact(Contact c) {
		
	}
}
