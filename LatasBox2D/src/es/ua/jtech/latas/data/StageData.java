package es.ua.jtech.latas.data;

import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class StageData {
	String stageName;
	Vector2 ballPosition;
	List<Vector2> canPositions;
	List<Vector3> coinPositions;
	List<Brick> brickList;
	String bgName;
	
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public List<Vector2> getCanPositions() {
		return canPositions;
	}
	public void setCanPositions(List<Vector2> canPositions) {
		this.canPositions = canPositions;
	}
	public Vector2 getBallPosition() {
		return ballPosition;
	}
	public void setBallPosition(Vector2 ballPosition) {
		this.ballPosition = ballPosition;
	}
	public List<Vector3> getCoinPositions() {
		return coinPositions;
	}
	public void setCoinPositions(List<Vector3> coinPositions) {
		this.coinPositions = coinPositions;
	}
	public List<Brick> getBrickList() {
		return brickList;
	}
	public void setBrickList(List<Brick> brickList) {
		this.brickList = brickList;
	}
	public String getBGName() {
		return bgName;
	}
	public void setBGName(String bgName) {
		this.bgName = bgName;
	}
}
