package com.example.mastermoviles.android_av_broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class MiBroadCastReceiver extends ActionBarActivity implements BroadcastReceiver{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals("android.intent.action.PHONE_STATE")){
            Bundle extras = intent.getExtras();
            if (extras != null) {
                String state = extras.getString(TelephonyManager.EXTRA_STATE);
                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    String phoneNumber = extras.getString(
                            TelephonyManager.EXTRA_INCOMING_NUMBER);
                    Log.i("DEBUG", phoneNumber);
                    send(phoneNumber, phoneNumber);
                }
            }
        }
    }

    public void send( String phoneNumber, String message )
    {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage( phoneNumber, null, message, null, null );
        Log.i( "SendSMS", "Enviando SMS a " + phoneNumber + ": \"" + message + "\"" );
    }
}
