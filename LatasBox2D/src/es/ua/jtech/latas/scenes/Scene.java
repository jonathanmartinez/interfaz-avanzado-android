package es.ua.jtech.latas.scenes;

import es.ua.jtech.latas.Game;

public interface Scene {

	public void init();
	
	public Game.SceneTransition update(float dt);
	
	public void render();
	
	public void resize(int w, int h);
	
	public void dispose();
}
