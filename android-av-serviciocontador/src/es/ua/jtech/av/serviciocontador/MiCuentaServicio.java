package es.ua.jtech.av.serviciocontador;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MiCuentaServicio extends Service {
	MiTarea miTarea;
    int cont;

	@Override
	public void onCreate() {
		super.onCreate();
		Toast.makeText(this,"Servicio creado ...", Toast.LENGTH_LONG).show();
		Log.i("SRV","onCreate");

		//TODO Crear una nueva MiTarea
        MiTarea t = new MiTarea();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("SRV", "onStartCommand");

		//TODO Ejecutar miTarea
        miTarea.execute();

		return Service.START_STICKY;
	}

	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this,"onDestroy: Servicio destruido ...", Toast.LENGTH_LONG).show();
		Log.i("SRV","Servicio destruido");
		
		//TODO Cancelar miTarea
        miTarea.cancel(true);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	
	private class MiTarea extends AsyncTask<String, String, String>{
		private int i; //Variable para la cuenta
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			//TODO inicializar la cuenta
            cont = 0;
		}

		@Override
		protected String doInBackground(String... params) {
			//TODO bucle que incremente la cuenta
			//hasta un valor máximo permitido (100).
			//En cada iteración publicaremos el progreso y haremos un
			//  Thread.sleep(5000).

            for(int i = 0; i<100; i++){
                onProgressUpdate(""+i);
                cont = i;
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {

                }
            }

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			//TODO Mostrar un Toast con el progreso de la cuenta
			//Nota: no mostar el valor del campo i, pues éste puede
			//haber cambiado en el momento de ejecutar este método
			//al tratarse de hilos diferentes. Muéstrese el
			//argumento values[0] en su lugar.
            Toast.makeText(this,"Contador: "+ values[0], Toast.LENGTH_LONG).show();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			
			//TODO Asignar valor máximo para que finalice la cuenta
            cont = 100;
		}
		
	}
}
