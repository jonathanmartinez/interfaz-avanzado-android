package es.ua.jtech.latas.data;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Brick {
	public Vector2 position;
	public Vector2 size;
	public Vector3 color;
	
	public Brick()
	{
		this.position = new Vector2();
		this.size = new Vector2();
		this.color = new Vector3();		
	}
	public Brick(Vector2 position, Vector2 size, Vector3 color)
	{
		this.position = position;
		this.size = size;
		this.color = color;
	}
}
