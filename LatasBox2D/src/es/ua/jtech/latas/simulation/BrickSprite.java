package es.ua.jtech.latas.simulation;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class BrickSprite extends Sprite {

	public BrickSprite(int width, int height, float r, float g, float b) {
		super(new Texture(width, height, Format.RGBA4444));
		this.setOrigin(width/2, height/2);
		Pixmap p = new Pixmap(width, height, Format.RGBA4444);
		p.setColor(r, g, b, 1);
		p.fill();
		this.getTexture().draw(p, 0, 0);
	}
}
