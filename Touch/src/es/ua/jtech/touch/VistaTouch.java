package es.ua.jtech.touch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;
import android.widget.TextView;

public class VistaTouch extends View {

	private final static int MEDIA_ANCHURA_RECTANGULO = 25;
	private final static int MEDIA_ALTURA_RECTANGULO = 25;

	float x, y;
	float vx, vy;
	boolean colorAzul;

	public VistaTouch(Context context) {
		super(context);

	}

	private boolean collidesRectangle(float ex, float ey) {
		return ex >= x - MEDIA_ANCHURA_RECTANGULO
				&& ex <= x + MEDIA_ANCHURA_RECTANGULO
				&& ey >= y - MEDIA_ALTURA_RECTANGULO
				&& ey <= y + MEDIA_ALTURA_RECTANGULO;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		x = this.getWidth() / 2;
		y = this.getHeight() / 2;
	}

	@Override
	protected void onDraw(Canvas canvas) {

		Paint paint = new Paint();
		if(colorAzul) {
			paint.setColor(Color.BLUE);			
		} else {
			paint.setColor(Color.RED);						
		}
		paint.setStyle(Style.FILL);

		canvas.drawColor(Color.WHITE);
		canvas.drawRect(x - MEDIA_ANCHURA_RECTANGULO, y - MEDIA_ALTURA_RECTANGULO,
				x + MEDIA_ANCHURA_RECTANGULO, y + MEDIA_ALTURA_RECTANGULO, paint);

		// Vector velocidad
		paint.setColor(Color.RED);
		canvas.drawLine(x, y, x+vx, y+vy, paint);
	}

		
	// TODO (1) Definir m�todo para procesar eventos de la pantalla t�ctil
    // TODO (2) Implementar reconocedor de gestos y delegar en �l
    GestureDetector detectorGestos;

    public ViewGestos(Context context) {
        super(context);

        ListenerGestos lg = new ListenerGestos();
        detectorGestos = new GestureDetector(lg);
        detectorGestos.setOnDoubleTapListener(lg);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return detectorGestos.onTouchEvent(event);
    }

    class ListenerGestos extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            if(event.getAction() == MotionEvent.ACTION_MOVE) {
                x = event.getX();
                y = event.getY();

                TextView t = (TextView)(findViewById(R.id.cuadrado));
                t.setX(x);
                t.setY(y);

                this.invalidate();
            }
            return true;
        }

        @Override
        public boolean onMove(MotionEvent e) {
            if(event.getAction() == MotionEvent.ACTION_MOVE) {
                x = event.getX();
                y = event.getY();

                this.invalidate();
            }
            return true;
        }


        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // Tratar el evento
            return true;
        }
    }
}
