public class MiWidget extends AppWidgetProvider
{
    public static final String ACTION_WIDGET_CLICK =
            "es.ua.jtech.av.ACTION_WIDGET_CLICK";

    private void actualizarWidget(Context context, AppWidgetManager appWidgetManager, int widgetId){
        RemoteViews updateViews = new RemoteViews(
                context.getPackageName(),
                R.layout.miwidget_layout);

//Seleccionar frase aleatoria
        String frases[] = context.getResources().getStringArray(R.array.frases);
        updateViews.setTextViewText(R.id.TextView01,
                frases[(int)(Math.random()*frases.length)]);

//Comportamiento del botón
//On-click listener que envía un broadcast intent
        Intent intent = new Intent(ACTION_WIDGET_CLICK);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context, widgetId,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        updateViews.setOnClickPendingIntent(R.id.miwidget, pendingIntent);

//Notificamos al manager de la actualización del widget actual
        appWidgetManager.updateAppWidget(widgetId, updateViews);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        //TODO actualizar
        actualizarWidget(context,appWidgetManager,appWidgetIds[0]);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if(intent.getAction().equals(ACTION_WIDGET_CLICK)){
            //TODO actualizar
            actualizarWidget(context,intent);
        }
    }
}