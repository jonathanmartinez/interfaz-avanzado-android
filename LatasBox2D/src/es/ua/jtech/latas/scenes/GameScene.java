package es.ua.jtech.latas.scenes;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import es.ua.jtech.latas.Game;
import es.ua.jtech.latas.GestureDetector;
import es.ua.jtech.latas.RenderUtils;
import es.ua.jtech.latas.Game.SceneTransition;
import es.ua.jtech.latas.GestureDetector.OnGestureListener;
import es.ua.jtech.latas.RenderUtils.TextHAlign;
import es.ua.jtech.latas.RenderUtils.TextVAlign;
import es.ua.jtech.latas.data.StageData;
import es.ua.jtech.latas.data.StringManager;
import es.ua.jtech.latas.simulation.BallSprite;
import es.ua.jtech.latas.simulation.BrickSprite;
import es.ua.jtech.latas.simulation.CanSprite;
import es.ua.jtech.latas.simulation.CoinSprite;
import es.ua.jtech.latas.simulation.Simulation;

public class GameScene implements Scene, OnGestureListener {

	private final static int SCORE_MUL_FACTOR = 100;
	private static final int MAX_SHOTS = 3;
	
	/**
	 * Estados del juego:
	 * - INPUT: Espera que el jugador lance la bola
	 * - SIMULATION: La bola se mueve e interactua con el resto de objetos de la escena
	 * - FINISHED: La bola ha parado de moverse y el usuario debe tocar la pantalla para continuar
	 */
	enum GameSceneState {
		GAME_SCENE_STATE_INPUT,
		GAME_SCENE_STATE_SIMULATION,
		GAME_SCENE_STATE_FINISHED
	}
	
	// Dimensiones del escenario
	int width;
	int height;
	
	// Estado actual del juego
	GameSceneState state;
	
	// Datos de la fase actual
	StageData stage;
	
	// Simulaci�n de f�sicas
	Simulation simulation;
	
	// Sprites del juego
	BallSprite ball;
	List<CanSprite> cans;
	List<CoinSprite> coins;
	List<BrickSprite> bricks;
	
	// Batch para dibujar texto y sprites
	SpriteBatch batch;
	
	// Detector de gestos para reconocer la entrada del usuario
	GestureDetector gesture;
	
	// Indica si estamos arrastrando la bola manualmente
	boolean isGrabbed = false;
	
	// Número de disparos
	int shots;
	
	// Textura de fondos
	Texture currentBG;
	
	@Override
	public void init() {
		width = Gdx.app.getGraphics().getWidth();
		height = Gdx.app.getGraphics().getHeight();
		
		// Almacena los datos de la fase actual
		int stageNum = Game.status.getStage();
		stage = Game.assets.stages.get(stageNum);
		
		// Inicializa la simulaci�n
		simulation = new Simulation(stage);
		ball = simulation.getBall();
		cans = simulation.getCans();
		coins = simulation.getCoins();
		bricks = simulation.getBricks();
		
		batch = new SpriteBatch();
		
		gesture = new GestureDetector(this);
		
		state = GameSceneState.GAME_SCENE_STATE_INPUT;
		
		shots = 0;
		
		if (Game.assets.stagesBG.containsKey(stage.getBGName()))
			currentBG = Game.assets.stagesBG.get(stage.getBGName());
		else
			currentBG = null;
	}
	
	@Override
	public void render() {
		RenderUtils.clearBackground(width, height);
		batch.begin();
		
		if (currentBG != null)
			batch.draw(currentBG, 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f, 1.0f, 0.0f);

		// Dibuja la bola
		ball.draw(batch);
		
		// Dibuja las latas
		for(CanSprite can: cans) {
			can.draw(batch);
		}
		
		// Dibuja las monedas
		for(CoinSprite coin: coins) {
			coin.draw(batch);
		}
		
		for(BrickSprite brick: bricks) {
			brick.draw(batch);
		}
		
		// Dibuja los marcadores
		RenderUtils.renderText(batch, stage.getStageName(), 0, height, TextHAlign.TEXT_H_ALIGN_LEFT, TextVAlign.TEXT_V_ALIGN_TOP);
		RenderUtils.renderText(batch, String.format("%d of %d shots", shots, MAX_SHOTS), 0, height-32, TextHAlign.TEXT_H_ALIGN_LEFT, TextVAlign.TEXT_V_ALIGN_TOP);
		RenderUtils.renderText(batch, "Damage: " + Game.status.getScoreString(), width, height, TextHAlign.TEXT_H_ALIGN_RIGHT, TextVAlign.TEXT_V_ALIGN_TOP);
		RenderUtils.renderText(batch, "Money: " + Game.status.getMoneyString(), width, height-32, TextHAlign.TEXT_H_ALIGN_RIGHT, TextVAlign.TEXT_V_ALIGN_TOP);
				
		// Dibuja el texto de inicio
		if(state==GameSceneState.GAME_SCENE_STATE_INPUT) {
			RenderUtils.renderText(batch, StringManager.getString(StringManager.ID_STRING_START), 
					width/2, height/2, TextHAlign.TEXT_H_ALIGN_CENTER, TextVAlign.TEXT_V_ALIGN_CENTER);			
		}
		
		// Dibuja el texto de finalizacion
		if(state==GameSceneState.GAME_SCENE_STATE_FINISHED) {
			RenderUtils.renderText(batch, StringManager.getString(StringManager.ID_STRING_END), 
					width/2, height/2, TextHAlign.TEXT_H_ALIGN_CENTER, TextVAlign.TEXT_V_ALIGN_CENTER);						
		}
		
		batch.end();
	}

	@Override
	public SceneTransition update(float dt) {
		float delta = Gdx.app.getGraphics().getDeltaTime();

		/*
		if(Gdx.input.isAccelerometerAvailable()) {
			simulation.setGravity(Gdx.input.getAccelerometerY(), -Gdx.input.getAccelerometerX());				
		}*/

		switch(state) {
		case GAME_SCENE_STATE_INPUT:
			// Lee entrada			
			if(Gdx.input.justTouched()) {
				gesture.touchDown(Gdx.input.getX(), Gdx.input.getY());
			} else if(Gdx.input.isTouched()) {
				gesture.touchMove(Gdx.input.getX(), Gdx.input.getY());				
			} else {
				gesture.touchNone();				
			}
			break;
		case GAME_SCENE_STATE_SIMULATION:
			// Actualiza la puntuacion
			Game.status.addScore(simulation.getCurrentDamage() * SCORE_MUL_FACTOR);
			simulation.resetCurrentDamage();
			// Pasa a estado finalizado cuando la bola para de moverse
			if(simulation.hasFinished()) {
				if (shots == MAX_SHOTS)
					state = GameSceneState.GAME_SCENE_STATE_FINISHED;
				else
					state = GameSceneState.GAME_SCENE_STATE_INPUT;
			}		
			break;
		case GAME_SCENE_STATE_FINISHED:
			// Al tocar la pantalla pasa a la siguiente fase
			if(Gdx.input.justTouched()) {
				Game.status.nextStage();
				// Si estamos en la �ltima fase, vuelve a la pantalla de t�tulo
				if(Game.status.getStage() == Game.assets.stages.size()) {
					return SceneTransition.TRANSITION_TITLE;
				} else {
					return SceneTransition.TRANSITION_GAME;
				}
			}
			break;
		}
 			
		// Actualiza las físicas
		if (simulation.updatePhysics(delta)) {
			// Si ha cambiado algo (desaparecido monedas) hay que recargar la lista de monedas
			coins = simulation.getCoins();
		}
		
		return SceneTransition.TRANSITION_NONE;
	}

	@Override
	public void dispose() {
		simulation.dispose();
		batch.dispose();
	}

	@Override
	public void resize(int w, int h) {
		// Especificar para permitir cambio de orientacion durante el juego
		// width = w;
		// height= h;
		// ...
	}

	// TODO Implementa el c�digo apropiado para cada gesto
	
	@Override
	public void onFling(int firstX, int firstY, int lastX, int lastY,
			float velocityX, float velocityY) {
		// TODO ?
	}

	@Override
	public void onDown(int x, int y) {
		// TODO ?
	}
	
	@Override
	public void onScrollMove(int firstX, int firstY, int lastX, int lastY,
			float distanceX, float distanceY) {
		// TODO ?
	}

	@Override
	public void onScrollUp(int firstX, int firstY, int lastX, int lastY) {
		// TODO ?
	}

	@Override
	public void onSingleTap(int x, int y) {
		// TODO ?
	}

}
