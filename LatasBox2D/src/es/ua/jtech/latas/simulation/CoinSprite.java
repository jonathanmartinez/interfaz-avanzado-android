package es.ua.jtech.latas.simulation;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class CoinSprite extends Sprite {

	public final static int COIN_RADIUS = 8; 
	float value;

	public CoinSprite(TextureRegion region, float value) {
		super(region);
		this.setOrigin(COIN_RADIUS, COIN_RADIUS);
		this.value = value;
	}
	
	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}
}
