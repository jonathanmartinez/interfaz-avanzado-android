package es.ua.jtech.latas.simulation;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class BallSprite extends Sprite {

	public final static int BALL_RADIUS = 16; 

	public BallSprite(TextureRegion region) {
		super(region);
		this.setOrigin(BALL_RADIUS, BALL_RADIUS);
	}
	
}
