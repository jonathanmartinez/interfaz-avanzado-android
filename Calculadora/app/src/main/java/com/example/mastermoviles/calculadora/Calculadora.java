package com.example.mastermoviles.calculadora;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


public class Calculadora extends Service {
    MiTarea miTarea;
    private final IBinder binder = new MiBinder();


    public int sumar(int a, int b){
        return a+b;
    }
    public int restar(int a, int b){
        return a-b;
    }
    public int multiplicar(int a, int b){
        return a*b;
    }
    public int dividir(int a, int b){
        return a/b;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Servicio creado ...", Toast.LENGTH_LONG).show();
        Log.i("SRV", "onCreate");

        //TODO Crear una nueva MiTarea
        MiTarea t = new MiTarea();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("SRV", "onStartCommand");

        //TODO Ejecutar miTarea
        miTarea.execute();

        return Service.START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"onDestroy: Servicio destruido ...", Toast.LENGTH_LONG).show();
        Log.i("SRV","Servicio destruido");

        //TODO Cancelar miTarea
        miTarea.cancel(true);
    }



    @Override
    public IBinder onBind(Intent intent){
        return binder;
    }

    public class MiBinder extends Binder {
        Calculadora getService() {
            return Calculadora.this;
        }
    }


    private class MiTarea extends AsyncTask<String, String, String> {

        public int sumar(int a, int b){
            return a+b;
        }
        public int restar(int a, int b){
            return a-b;
        }
        public int multiplicar(int a, int b){
            return a*b;
        }
        public int dividir(int a, int b){
            return a/b;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {


            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();


        }



    }
}
