package es.ua.jtech.latas.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

	private final static String PACKED_TEXTURES = "datos/pack";
	private final static String PACKED_REGION_BALL = "bola";
	private final static String PACKED_REGION_CAN = "lata";
	private final static String PACKED_REGION_COIN = "moneda";
	
	private final static String BITMAP_FONT_FILENAME = "datos/fuente.fnt";
	private final static String STAGE_DATA_FILENAME = "datos/stages.json";

	TextureAtlas atlas;
	
	public TextureRegion ballRegion;
	public TextureRegion canRegion;
	public TextureRegion coinRegion;
	public Map<String, Texture> stagesBG;
	
	public BitmapFont font;
	
	public List<StageData> stages;

	public void load() {
		atlas = new TextureAtlas(Gdx.files.getFileHandle(PACKED_TEXTURES, 
				FileType.Internal));
		ballRegion = atlas.findRegion(PACKED_REGION_BALL);
		canRegion = atlas.findRegion(PACKED_REGION_CAN);
		coinRegion = atlas.findRegion(PACKED_REGION_COIN);
		
		font = new BitmapFont(Gdx.files.getFileHandle(BITMAP_FONT_FILENAME,
				FileType.Internal), false);

		stages = StageDataLoader.load(STAGE_DATA_FILENAME);
		stagesBG = new HashMap<String, Texture>();
		for (StageData stage: stages) {
			String bgName = stage.getBGName();
			if (bgName != null)
				stagesBG.put(bgName, new Texture(Gdx.files.getFileHandle(bgName, FileType.Internal)));
		}
	}

	public void dispose() {
		font.dispose();		
		atlas.dispose();
		for (Map.Entry<String, Texture> entry : stagesBG.entrySet()) {
			entry.getValue().dispose();
		}
	}
}
