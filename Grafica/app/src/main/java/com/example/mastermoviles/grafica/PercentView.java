package com.example.mastermoviles.grafica;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mastermoviles on 14/12/14.
 */





public class PercentView extends View {
    float percentage = 0.25f;
    public PercentView (Context context) {
        super(context);
        init();
    }
    public PercentView (Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public PercentView (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        TypedArray ta = this.getContext().obtainStyledAttributes(attrs,
                R.styleable.Grafica);
        this.percentage = ta.getFloat(R.styleable.Grafica_percentage, 0);
    }
    private void init() {
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        bgpaint = new Paint();
        bgpaint.setColor(Color.RED);
        bgpaint.setAntiAlias(true);
        bgpaint.setStyle(Paint.Style.FILL);

        // --- SHADER ---
        /*Shader s = new Shader();
        bgpaint.setShader(s);*/

        rect = new RectF();
    }
    Paint paint;
    Paint bgpaint;
    RectF rect;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //draw background circle anyway
        int left = 0;
        int width = 100;//getWidth();
        int top = 0;
        rect.set(left, top, left+width, top + width);
        canvas.drawArc(rect, -90, 360, true, bgpaint);
        if(percentage!=0) {
            canvas.drawArc(rect, -90, (360*percentage), true, paint);
        }
    }
    public void setPercentage(float percentage) {
        this.percentage = percentage / 100;
        invalidate();
    }
}