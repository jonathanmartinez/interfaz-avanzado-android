package es.ua.jtech.av.localizador;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class Localizador extends BroadcastReceiver {
	private static final String cadena="Localizador";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		Log.i("Localizador", "onReceive");
		if (extras == null) {
			return; // nothing to process
		}
		Log.i("Localizador", "Algo recibido");

		Object[] pdus = (Object[]) extras.get("pdus");
		for (int i = 0; i < pdus.length; i++) {
			Log.i("Localizador", "pdu " + i);
			SmsMessage mensajeEntrante = SmsMessage
					.createFromPdu((byte[]) pdus[i]);
			String remitente = mensajeEntrante.getOriginatingAddress();

			Toast.makeText(
					context,
					"Recibido mensaje de " + remitente + ": \""
							+ mensajeEntrante.getMessageBody() + "\"",
					Toast.LENGTH_LONG).show();

			if (mensajeEntrante.getMessageBody().trim().equalsIgnoreCase(cadena)) {
				Log.i("Localizador", "petición de coordenadas");
				Location localizacion = obtenerLocalizacion(context);
				String direccion = obtenerDireccion(context, localizacion);
				if (localizacion != null) {
					String mensajeSaliente = String
							.format("Por ultimo estuve en: %1$s (lat:%2$2.6f, long:%3$2.6f)",
									direccion,
									localizacion.getLatitude(),
									localizacion.getLongitude());
					enviarSMS(context, remitente, mensajeSaliente);
				} else {
					enviarSMS(context, remitente,
							"No consigo obtener la localizacion.");
				}
			}

		}
	}

	public Location obtenerLocalizacion(Context context) {
		LocationManager locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String bestProvider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(bestProvider);
		if (location == null) {
			criteria.setAccuracy(Criteria.ACCURACY_COARSE);
			bestProvider = locationManager.getBestProvider(criteria, true);
			location = locationManager.getLastKnownLocation(bestProvider);
		}
		return location;
	}

	private String obtenerDireccion(Context context,
			Location lastKnownLocation) {
		if (lastKnownLocation == null)
			return null;
		String address = "";
		Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
		List<Address> addresses;
		try {
			addresses = geoCoder.getFromLocation(
					lastKnownLocation.getLatitude(),
					lastKnownLocation.getLongitude(), 1);
			if (addresses.size() > 0) {
				for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
					String addressLine = addresses.get(0).getAddressLine(i);
					if (addressLine != null) {
						address += addresses.get(0).getAddressLine(i) + "\n";
					}
				}
			}
		} catch (IOException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
		}
		return address;

	}

	private void enviarSMS(Context context, String requestor,
			String message) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(requestor, null, message, null, null);
		Toast.makeText(context,
				"Enviando mensaje a  " + requestor + ": \"" + message + "\"",
				Toast.LENGTH_LONG).show();
	}

}