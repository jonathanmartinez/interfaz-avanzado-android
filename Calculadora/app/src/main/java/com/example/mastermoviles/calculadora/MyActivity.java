package com.example.mastermoviles.calculadora;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MyActivity extends ActionBarActivity {
    private Calculadora servicio; //La referencia al servicio

    int operando1;
    int operando2;
    Button sumar;
    Button restar;
    Button multiplicar;
    Button dividir;

    private ServiceConnection serviceConnection =
            new ServiceConnection()
            {
                @Override
                public void onServiceConnected(
                        ComponentName className, IBinder service) {
                    servicio = ((Calculadora.MiBinder)service).getService();
                }
                @Override
                public void onServiceDisconnected(ComponentName className) {
                    servicio = null;
                }
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        sumar = (Button)(findViewById(R.id.sumar));
        sumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText op1 = (EditText)(findViewById(R.id.operando1));
                EditText op2 = (EditText)(findViewById(R.id.operando2));

                servicio.sumar(Integer.parseInt(op1.getText().toString()), Integer.parseInt(op2.getText().toString()));
            }
        });

        restar = (Button)(findViewById(R.id.restar));
        restar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText op1 = (EditText)(findViewById(R.id.operando1));
                EditText op2 = (EditText)(findViewById(R.id.operando2));

                servicio.restar(Integer.parseInt(op1.getText().toString()), Integer.parseInt(op2.getText().toString()));
            }
        });

        multiplicar = (Button)(findViewById(R.id.multiplicar));
        multiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText op1 = (EditText)(findViewById(R.id.operando1));
                EditText op2 = (EditText)(findViewById(R.id.operando2));

                servicio.multiplicar(Integer.parseInt(op1.getText().toString()), Integer.parseInt(op2.getText().toString()));
            }
        });

        dividir = (Button)(findViewById(R.id.dividir));
        dividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText op1 = (EditText)(findViewById(R.id.operando1));
                EditText op2 = (EditText)(findViewById(R.id.operando2));

                servicio.dividir(Integer.parseInt(op1.getText().toString()), Integer.parseInt(op2.getText().toString()));
            }
        });

        Intent intent = new Intent(MyActivity.this,
                Calculadora.class);
        bindService(intent, serviceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
