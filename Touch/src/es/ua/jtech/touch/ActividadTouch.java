package es.ua.jtech.touch;

import android.app.Activity;
import android.os.Bundle;

public class ActividadTouch extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new VistaTouch(this));
    }
}